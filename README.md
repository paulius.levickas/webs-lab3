# Product storage

## Launch the app

Follow steps below in order to launch the application:

1. ```docker-compose up -d``` - to build and run the application.

## How to use the api

### Get all products

```aidl
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:getProductsRequest></wsdl:getProductsRequest>
   </soapenv:Body>
</soapenv:Envelope>

```

### Get one product by id

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:getProductRequest>
         <wsdl:id>0</wsdl:id>
      </wsdl:getProductRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

### Delete product by id

```aidl
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:deleteProductRequest>
         <wsdl:id>0</wsdl:id>
      </wsdl:deleteProductRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

### Create Product 

```aidl
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:createProductRequest>
         <wsdl:description>ilga sofa</wsdl:description>
         <wsdl:name>Sofa</wsdl:name>
      </wsdl:createProductRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

### Update product

```aidl
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:updateProductRequest>
         <wsdl:id>0</wsdl:id>
         <wsdl:description>trumpa sofa</wsdl:description>
         <wsdl:name>Sofa2</wsdl:name>
      </wsdl:updateProductRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

### Add user to Product

```$xslt
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:putUserToProductRequest>
         <wsdl:id>0</wsdl:id>
         <wsdl:email>paulius@gmail.com</wsdl:email>
      </wsdl:putUserToProductRequest>
   </soapenv:Body>
</soapenv:Envelope>
'
```

### GET - get all users

```$xslt
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://www.kurti.lt/products/wsdl">
   <soapenv:Header/>
   <soapenv:Body>
      <wsdl:getUsersRequest></wsdl:getUsersRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

author Paulius Levickas