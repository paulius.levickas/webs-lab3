package com.example.demo;

public class Constants {

    public static final String NAMESPACE_URI = "http://www.kurti.lt/products/wsdl";
    public static final String ERROR_CODE = "ERROR";
    public static final String NOT_FOUND = "Not Found";
    public static final String INVALID_REQUEST = "Invalid Request";
    public static final String PRODUCT_DELETED_SUCCESSFULLY = "Product Deleted Successfully";
    public static final String USER_FOR_DEFECT_DELETED_SUCCESSFULLY = "User for Defect Deleted Successfully";

    private final static String URL_BASE = "http://friend:5000";
    public final static String BASE_USERS_URI = URL_BASE + "/users";
}
