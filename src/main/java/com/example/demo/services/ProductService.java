package com.example.demo.services;


import lombok.RequiredArgsConstructor;
import lt.kurti.products.wsdl.CreateProductRequest;
import lt.kurti.products.wsdl.GetProductsRequest;
import lt.kurti.products.wsdl.Product;
import lt.kurti.products.wsdl.PutUserToProductRequest;
import lt.kurti.products.wsdl.UpdateProductRequest;
import lt.kurti.products.wsdl.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    List<Product> products = new ArrayList<>();
    int id = 0;

    public ProductService(final UserService userService) {
        this.userService = userService;

        Product product1 = new Product();
        Product product2 = new Product();
        Product product3 = new Product();

        product1.setDescription("prod1");
        product1.setName("prod1");
        product1.setId(0);

        product2.setDescription("prod2");
        product2.setName("prod2");
        product2.setId(1);

        product3.setDescription("prod3");
        product3.setName("prod3");
        product3.setId(2);

        products.add(product1);
        products.add(product2);
        products.add(product3);

    }

    private final UserService userService;

    public Product createProduct(CreateProductRequest createProductRequest) {
        Product product = new Product();
        product.setDescription(createProductRequest.getDescription());
        product.setName(createProductRequest.getName());
        product.setId(id);

        if(createProductRequest.getUserEmail() != null && !createProductRequest.getUserEmail().equals("")){
            User user = new User();
            user.setEmail(createProductRequest.getUserEmail());

            User dbUser = userService.saveEmployee(user);
            product.setEmployee(dbUser);
        }


        id++;

        products.add(product);
        return product;
    }

    public List<Product> getProducts(GetProductsRequest getProductsRequest) {
        if(getProductsRequest.getId() == null || getProductsRequest.getId().equals("")) {
            return products;
        } else {
            for(Product product : products) {
                if(product.getId() == Integer.parseInt(getProductsRequest.getId())) {
                    return List.of(product);
                }
            }
        }

        return null;
    }

    public Product getProductById(int id) {
        for(Product product : products) {
            if(product.getId() == id) {
                return product;
            }
        }

        return null;
    }

    public void deleteProductById(int id) {
        products = products.stream().filter(product -> product.getId() != id).collect(Collectors.toList());
    }

    public Product updateProduct(UpdateProductRequest updateProductRequest) {
        Product product = getProductById(updateProductRequest.getId());
        if(product == null) {
            return null;
        }

        deleteProductById(updateProductRequest.getId());

        product.setName(updateProductRequest.getName());
        product.setDescription(updateProductRequest.getDescription());
        products.add(product);

        return product;
    }

    public Product putUserToProduct(PutUserToProductRequest putUserToProductRequest) {
        Product product = getProductById(putUserToProductRequest.getId());

        if(product == null) {
            return null;
        }

        User user = new User();
        user.setEmail(putUserToProductRequest.getEmail());

        User dbUser = userService.saveEmployee(user);
        product.setEmployee(dbUser);

        // Update product in list
        deleteProductById(putUserToProductRequest.getId());
        products.add(product);
        return product;
    }

}
