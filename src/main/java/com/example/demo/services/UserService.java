package com.example.demo.services;

import com.example.demo.models.UserResponse;
import com.example.demo.models.UsersResponse;
import lombok.extern.log4j.Log4j2;
import lt.kurti.products.wsdl.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.example.demo.Constants.BASE_USERS_URI;

@Service
@Log4j2
public class UserService {

    public List<User> getEmployees() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            UsersResponse usersResponse = restTemplate.getForObject(BASE_USERS_URI, UsersResponse.class);
            return usersResponse.getData();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public User saveEmployee(User user) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<User> request = new HttpEntity<>(user);
        try {
            UserResponse userResponse = restTemplate.postForObject(BASE_USERS_URI, request, UserResponse.class);
            log.info("SUCCESS saving user");
            return userResponse.getData();
        } catch (HttpClientErrorException e) {
            if(e.getStatusCode() == HttpStatus.CONFLICT) {
                return user;
            }
            e.printStackTrace();
            log.error("ERROR saving user");
            return null;
        }
    }

}
