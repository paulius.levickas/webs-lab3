package com.example.demo.endpoint;

import com.example.demo.exceptions.ServiceFault;
import com.example.demo.exceptions.ServiceFaultException;
import com.example.demo.services.ProductService;
import com.example.demo.services.UserService;
import lombok.RequiredArgsConstructor;
import lt.kurti.products.wsdl.CreateProductRequest;
import lt.kurti.products.wsdl.DeleteProductRequest;
import lt.kurti.products.wsdl.DeleteProductResponse;
import lt.kurti.products.wsdl.GetProductRequest;
import lt.kurti.products.wsdl.GetProductResponse;
import lt.kurti.products.wsdl.GetProductsRequest;
import lt.kurti.products.wsdl.GetProductsResponse;
import lt.kurti.products.wsdl.GetUsersResponse;
import lt.kurti.products.wsdl.Product;
import lt.kurti.products.wsdl.PutUserToProductRequest;
import lt.kurti.products.wsdl.UpdateProductRequest;
import lt.kurti.products.wsdl.User;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

import static com.example.demo.Constants.PRODUCT_DELETED_SUCCESSFULLY;
import static com.example.demo.Constants.ERROR_CODE;
import static com.example.demo.Constants.NAMESPACE_URI;
import static com.example.demo.Constants.NOT_FOUND;

@RequiredArgsConstructor
@Endpoint
public class ProductEndpoint {

    private final ProductService productService;

    private final UserService userService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createProductRequest")
    @ResponsePayload
    public GetProductResponse addProduct(@RequestPayload CreateProductRequest createProductRequest) {
            final Product product = productService.createProduct(createProductRequest);

            GetProductResponse getProductResponse = new GetProductResponse();
            getProductResponse.setProduct(product);

            return getProductResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateProductRequest")
    @ResponsePayload
    public GetProductResponse updateProduct(@RequestPayload UpdateProductRequest updateProductRequest) {
        final Product product = productService.updateProduct(updateProductRequest);

        if (product == null) {
            throw new ServiceFaultException(ERROR_CODE, new ServiceFault(NOT_FOUND, "PRODUCT_NOT_FOUND_BY_ID"));
        }

        GetProductResponse getProductResponse = new GetProductResponse();
        getProductResponse.setProduct(product);

        return getProductResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getProductsRequest")
    @ResponsePayload
    public GetProductsResponse getProducts(@RequestPayload GetProductsRequest getProductsRequest) {
        GetProductsResponse response = new GetProductsResponse();
        final List<Product> result = productService.getProducts(getProductsRequest);
        result.forEach(product -> response.getProduct().add(product));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getProductRequest")
    @ResponsePayload
    public GetProductResponse getDefectById(@RequestPayload GetProductRequest getProductRequest) {
        final Product product = productService.getProductById(getProductRequest.getId());

        if (product == null) {
            throw new ServiceFaultException(ERROR_CODE, new ServiceFault(NOT_FOUND, "PRODUCT_NOT_FOUND_BY_ID"));
        }

        GetProductResponse getDefectResponse = new GetProductResponse();
        getDefectResponse.setProduct(product);
        return getDefectResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteProductRequest")
    @ResponsePayload
    public DeleteProductResponse deleteDefect(@RequestPayload DeleteProductRequest deleteDefectRequest) {
        productService.deleteProductById(deleteDefectRequest.getId());

        final DeleteProductResponse deleteDefectResponse = new DeleteProductResponse();
        deleteDefectResponse.setMessage(PRODUCT_DELETED_SUCCESSFULLY);

        return deleteDefectResponse;
    }

    /**
     * USER ENDPOINTS
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "putUserToProductRequest")
    @ResponsePayload
    public GetProductResponse putUserToProduct(@RequestPayload PutUserToProductRequest putUserToProductRequest) {
        final Product product = productService.putUserToProduct(putUserToProductRequest);

        if (product == null) {
            throw new ServiceFaultException(ERROR_CODE, new ServiceFault(NOT_FOUND, "PRODUCT_NOT_FOUND_BY_ID"));
        }

        GetProductResponse getProductResponse = new GetProductResponse();
        getProductResponse.setProduct(product);

        return getProductResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUsersRequest")
    @ResponsePayload
    public GetUsersResponse getUsers() {
        GetUsersResponse response = new GetUsersResponse();
        final List<User> result = userService.getEmployees();
        result.forEach(user -> response.getUser().add(user));

        return response;
    }


}

